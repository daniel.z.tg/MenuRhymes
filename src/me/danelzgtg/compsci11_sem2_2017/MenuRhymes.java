package me.danelzgtg.compsci11_sem2_2017;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * A program to select rhymes for numbers.
 * 
 * @author Daniel Tang
 * @since 16 February 2017
 */
public final class MenuRhymes {

	/**
	 * The prompt layout for the target rhyme number.
	 */
	private static final String NUMBER_PROMPT = "What whole number do you want to rhyme? : ";

	/**
	 * The error message when the user did not enter a valid integer.
	 */
	private static final String NUMBER_ERROR_MSG = "Sorry! I didn't understand that whole number, please try again.";

	/**
	 * The error message when the program did not have any rhymes for the specified integer.
	 */
	private static final String NUMBER_NO_RHYME = "I didn't have a rhyme for that!";

	/**
	 * The prompt layout for whether to run the rhyming selecting and output again.
	 */
	private static final String CONTINUE_PROMPT = "Do you want to play again? (y/n): ";

	/**
	 * THe error message when the user's intention for going again was not understood.
	 */
	private static final String CONTINUE_ERR_MSG = "Sorry! I didn't understand that choice, please try again.";

	/**
	 * The lowercase list of aliases that the user could choose to repeat with.
	 */
	private static final List<String> BOOLEAN_NAMES_TRUE = Collections.unmodifiableList(Arrays.asList(new String[] {
			"true", "yes", "t", "y", "yea"
	}));

	/**
	 * The lowercase list of aliases that hte user could choose to not repeat with.
	 */
	private static final List<String> BOOLEAN_NAMES_FALSE = Collections.unmodifiableList(Arrays.asList(new String[] {
			"false", "no", "f", "n", "nay"
	}));

	public static void main(final String[] ignore) {
		try (final Scanner scanner = new Scanner(System.in)) {
			boolean running = true;

			while (running) { // Support for playing again
				final int choice = promptInt(scanner, NUMBER_PROMPT, NUMBER_ERROR_MSG); // Obtain a number

				switch (choice) { // Choose a case based on the input, then print out the rhyme (if any)
				case 1:
					System.out.println("1, time to have some fun!");
					break;
				case 2:
					System.out.println("2, buckle my shoe!");
					break;
				case 3:
					System.out.println("3, I live in a tree!");
					break;
				case 4:
					System.out.println("4, open the door!");
					break;
				case 5:
					System.out.println("5, watch out for the bee hive!");
					break;
				case 6:
					System.out.println("6, pick up sticks!");
					break;
				case 7:
					System.out.println("7, add four to get eleven!");
					break;
				case 8:
					System.out.println("8, you are great!");
					break;
				case 9:
					System.out.println("9, that's just fine!");
					break;
				default:
					// No rhyme :(
					System.out.println(NUMBER_NO_RHYME);
					// break;
				}

				// Ask if the user wants to play again
				running = promptBoolean(scanner, CONTINUE_PROMPT, CONTINUE_ERR_MSG);
			}
		}
	}

	/**
	 * Obtains a yes/no answer from the user.
	 * 
	 * @param scanner The scanner to obtain input with.
	 * @param prompt The prompt message to present the user with.
	 * @param errorMsg The error message to show to the user if we did not understand.
	 * @return {@code true} if the user responded yes.
	 */
	private static final boolean promptBoolean(final Scanner scanner, final String prompt, final String errorMsg) {
		while (true) {
			System.out.print(prompt);
			final String input = scanner.nextLine().toLowerCase();

			if (BOOLEAN_NAMES_TRUE.contains(input)) {
				return true;
			} else if (BOOLEAN_NAMES_FALSE.contains(input)) {
				return false;
			} else {
				System.out.println(errorMsg);
			}
		}
	}

	/**
	 * Obtains a whole number answer from the user.
	 * 
	 * @param scanner The scanner to obtain input with.
	 * @param prompt The prompt message to present the user with.
	 * @param errorMsg The error message to show to the user if we did not understand.
	 * @return The {@code int} that was obtained.
	 */
	private static final int promptInt(final Scanner scanner, final String prompt, final String errorMsg) {
		while (true) {
			System.out.print(prompt);
			final String input = scanner.nextLine();

			try {
				return Integer.parseInt(input);
			} catch (final NumberFormatException nfe) {
				System.out.println(errorMsg);
			}
		}
	}

	private MenuRhymes() { throw new UnsupportedOperationException(); }
}
